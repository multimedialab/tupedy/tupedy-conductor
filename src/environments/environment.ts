// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAJnGxL1qPzr-_FEGd210Tw02sNLT3rcjI",
    authDomain: "tupedy-f2fcf.firebaseapp.com",
    databaseURL: "https://tupedy-f2fcf.firebaseio.com",
    projectId: "tupedy-f2fcf",
    storageBucket: "tupedy-f2fcf.appspot.com",
    messagingSenderId: "114267848906",
    appId: "1:114267848906:web:3c693d34a42b2669663433",
    measurementId: "G-VW9W91QEWZ"
  },
  onesignal: {
    appId: '6d48c404-60ab-4e4c-bde7-d227ff7420f2',
    googleProjectNumber: '114267848906',
    restKey: 'NmY1ZDQzMGItNTQ4ZC00YjAwLTg3YzItNWNmMWQ4YjQ5ZWRi'
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
