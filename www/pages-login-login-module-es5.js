function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html":
  /*!***********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html ***!
    \***********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesLoginLoginPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-content class=\"ion-padding\">\r\n  <div class=\"login-logo\">\r\n    <img src=\"assets/imgs/icon.png\" class=\"logo_icon\" alt=\"foodies\">\r\n    <p class=\"login-name\">{{'Login' | translate}}</p>\r\n    <p class=\"subTitle\">{{'Enter your login detail to' | translate}} <br>\r\n      {{'access your account' | translate}}</p>\r\n  </div>\r\n \r\n  <ion-list >\r\n    <ion-list-header>\r\n      <ion-label>\r\n        {{'Login With?' | translate}}\r\n      </ion-label>\r\n    </ion-list-header>\r\n  \r\n    <ion-item>\r\n      <ion-label *ngIf=\"isEmail\">{{'Email' | translate}}</ion-label>\r\n      <ion-label *ngIf=\"isPhone\">{{'Phone' | translate}}</ion-label>\r\n      <ion-select value=\"mail\" okText=\"Okay\" (ionChange)=\"changeMethod($event)\" cancelText=\"Dismiss\">\r\n        <ion-select-option value=\"mail\"  >{{'Email' | translate}}</ion-select-option>\r\n        <ion-select-option value=\"phone\" >{{'Log In Phone' | translate}}</ion-select-option>\r\n      </ion-select>\r\n    </ion-item>  \r\n  </ion-list>\r\n\r\n  <form #loginForm=\"ngForm\" novalidate>\r\n    <ion-list lines=\"none\" *ngIf=\"isEmail\">\r\n      <ion-item lines=\"none\">\r\n        <ion-input [(ngModel)]=\"login.email\" type=\"email\" [placeholder]=\"('Email' | translate )\" name=\"email\" #email=\"ngModel\"\r\n          spellcheck=\"false\" autocapitalize=\"off\" required>\r\n        </ion-input>\r\n      </ion-item>\r\n      <ion-text color=\"danger\">\r\n        <p [hidden]=\"email.valid || submitted == false\" class=\"ion-padding-start\">\r\n          {{'Email is required' | translate}}\r\n        </p>\r\n      </ion-text>\r\n\r\n      <ion-item lines=\"none\">\r\n        <ion-input [(ngModel)]=\"login.password\" name=\"password\" [placeholder]=\"('Password' | translate )\" type=\"password\"\r\n          #password=\"ngModel\" required>\r\n        </ion-input>\r\n      </ion-item>\r\n                 \r\n      <ion-text color=\"danger\">\r\n        <p [hidden]=\"password.valid || submitted == false\" class=\"ion-padding-start\">\r\n          {{'Password is required' | translate}}\r\n        </p>\r\n      </ion-text>\r\n    </ion-list>\r\n    <ion-list lines=\"none\" *ngIf=\"isPhone\">\r\n      <ion-item lines=\"none\" >\r\n        <ion-input [(ngModel)]=\"phone\" name=\"phone\" [placeholder]=\"('Phone' | translate )\" type=\"number\"\r\n          #password=\"ngModel\" required>\r\n        </ion-input>\r\n      </ion-item>      \r\n      <ion-item lines=\"none\" *ngIf=\"isVerification\" >\r\n        <ion-input [(ngModel)]=\"userCode\" name=\"userCode\" [placeholder]=\"('Verification code' | translate )\" type=\"number\"\r\n          #password=\"ngModel\" required>\r\n        </ion-input>\r\n      </ion-item>\r\n    </ion-list>\r\n\r\n    <div style=\"display: flex; justify-content: center; margin-top: 20px;\" id=\"recaptcha-container\" ></div>\r\n   \r\n\r\n    <ion-row class=\"ion-no-margin ion-no-padding\">\r\n      <ion-col class=\"ion-no-margin ion-no-padding\">\r\n        <p class=\"frgTag\" (click)=\"resetPass()\">{{'Forgot Password?' | translate}}</p>\r\n        <ion-button *ngIf=\"isEmail\" class=\"btn_class\" (click)=\"onLogin(loginForm)\" type=\"submit\" expand=\"block\" [disabled]=\"isLogin\">\r\n          <span *ngIf=\"!isLogin\"> {{'Log In' | translate}}</span>\r\n          <ion-spinner name=\"circles\" *ngIf=\"isLogin\"></ion-spinner>\r\n        </ion-button>    \r\n        <ion-button class=\"btn_class\" *ngIf=\"isPhone\" (click)=\"onLoginPhone(loginForm)\" type=\"button\" expand=\"block\">\r\n          <span>Iniciar Sesion</span>\r\n          <ion-spinner name=\"circles\" *ngIf=\"isLogin\"></ion-spinner>\r\n        </ion-button>    \r\n        <ion-button *ngIf=\"isVerification\" class=\"btn_class\" (click)=\"validateCode()\" type=\"button\" expand=\"block\">\r\n          <span> verificar codigo</span>\r\n          <ion-spinner name=\"circles\" *ngIf=\"isLogin\"></ion-spinner>\r\n        </ion-button>    \r\n      </ion-col>\r\n    </ion-row>\r\n    <div class=\"btns\">\r\n      <img src=\"assets/imgs/en.png\" (click)=\"changeLng('en')\"\r\n        [ngClass]=\"getClassName() == 'en' ? 'flagActive':'flagDeactive'\" alt=\"\">\r\n      <img src=\"assets/imgs/spanish.png\" (click)=\"changeLng('spanish')\"\r\n        [ngClass]=\"getClassName() == 'spanish' ? 'flagActive':'flagDeactive'\" alt=\"\">\r\n    </div>\r\n  </form>";
    /***/
  },

  /***/
  "./src/app/pages/login/login-routing.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/pages/login/login-routing.module.ts ***!
    \*****************************************************/

  /*! exports provided: LoginPageRoutingModule */

  /***/
  function srcAppPagesLoginLoginRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function () {
      return LoginPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./login.page */
    "./src/app/pages/login/login.page.ts");

    var routes = [{
      path: '',
      component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
    }];

    var LoginPageRoutingModule = function LoginPageRoutingModule() {
      _classCallCheck(this, LoginPageRoutingModule);
    };

    LoginPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], LoginPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/pages/login/login.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/pages/login/login.module.ts ***!
    \*********************************************/

  /*! exports provided: LoginPageModule */

  /***/
  function srcAppPagesLoginLoginModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LoginPageModule", function () {
      return LoginPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./login-routing.module */
    "./src/app/pages/login/login-routing.module.ts");
    /* harmony import */


    var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./login.page */
    "./src/app/pages/login/login.page.ts");
    /* harmony import */


    var src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/shared/shared.module */
    "./src/app/shared/shared.module.ts");

    var LoginPageModule = function LoginPageModule() {
      _classCallCheck(this, LoginPageModule);
    };

    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"], src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]],
      declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })], LoginPageModule);
    /***/
  },

  /***/
  "./src/app/pages/login/login.page.scss":
  /*!*********************************************!*\
    !*** ./src/app/pages/login/login.page.scss ***!
    \*********************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesLoginLoginPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-header {\n  --box-shadow:none;\n}\n\nion-footer {\n  --box-shadow:none;\n}\n\nion-toolbar {\n  border-color: none !important;\n}\n\nion-toolbar ion-title {\n  --color: white;\n}\n\n.login-logo {\n  text-align: center;\n  margin: auto;\n  right: 0px;\n  left: 0px;\n  margin-top: 30%;\n  margin-bottom: 20px;\n}\n\n.login-logo .logo_icon {\n  width: 110px !important;\n}\n\n.login-logo .login-name {\n  margin: 0px;\n  font-size: 1.5rem;\n}\n\n.login-logo .subTitle {\n  margin: 0px;\n  font-size: 1rem;\n  margin-top: 10px;\n}\n\n.frgTag {\n  text-align: right;\n  color: var(--ion-color-primary);\n}\n\n.btn_class {\n  color: white;\n  height: 50px;\n  --border-radius: 5px;\n  font-weight: 600;\n}\n\n.login-logo img {\n  max-width: 150px;\n}\n\n.list {\n  margin-bottom: 0;\n}\n\nion-list {\n  --ion-background-color:transparent;\n  margin: 0px;\n}\n\nion-item {\n  --ion-background-color:#f3f3f3;\n  border-radius: 5px !important;\n  margin-top: 10px;\n}\n\n.btns {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n          flex-direction: row;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  padding: 10px 0px;\n}\n\n.btns .flagActive {\n  height: 50px;\n  width: 50px;\n  border-radius: 50%;\n  border: 2px solid var(--ion-color-primary);\n  margin: 0px 10px;\n}\n\n.btns .flagDeactive {\n  height: 45px;\n  width: 45px;\n  margin: 0px 10px;\n  opacity: 0.5;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbG9naW4vRDpcXFJlcG9zaXRvcmlvc1xcVHVQZWR5XFx0dXBlZHlEcml2ZXJzL3NyY1xcYXBwXFxwYWdlc1xcbG9naW5cXGxvZ2luLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7QUNDSjs7QURDQTtFQUNJLGlCQUFBO0FDRUo7O0FEQUE7RUFDSSw2QkFBQTtBQ0dKOztBREZJO0VBQ0ksY0FBQTtBQ0lSOztBRERBO0VBQ0ksa0JBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7QUNJSjs7QURISTtFQUNJLHVCQUFBO0FDS1I7O0FESEk7RUFDSSxXQUFBO0VBQ0EsaUJBQUE7QUNLUjs7QURISTtFQUNJLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNLUjs7QURGQTtFQUNRLGlCQUFBO0VBQ0EsK0JBQUE7QUNLUjs7QURIQTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQkFBQTtBQ01KOztBREpBO0VBQ0ssZ0JBQUE7QUNPTDs7QURMQTtFQUNJLGdCQUFBO0FDUUo7O0FETkE7RUFDSSxrQ0FBQTtFQUNBLFdBQUE7QUNTSjs7QURQQTtFQUNLLDhCQUFBO0VBQ0QsNkJBQUE7RUFDQSxnQkFBQTtBQ1VKOztBRFBBO0VBQ0ksb0JBQUE7RUFBQSxhQUFBO0VBQ0EsOEJBQUE7RUFBQSw2QkFBQTtVQUFBLG1CQUFBO0VBQ0Esd0JBQUE7VUFBQSx1QkFBQTtFQUNBLHlCQUFBO1VBQUEsbUJBQUE7RUFDQSxpQkFBQTtBQ1VKOztBRFRJO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLDBDQUFBO0VBQ0EsZ0JBQUE7QUNXUjs7QURUSTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FDV1IiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taGVhZGVye1xuICAgIC0tYm94LXNoYWRvdzpub25lO1xufVxuaW9uLWZvb3RlcntcbiAgICAtLWJveC1zaGFkb3c6bm9uZTtcbn1cbmlvbi10b29sYmFye1xuICAgIGJvcmRlci1jb2xvcjogbm9uZSAhaW1wb3J0YW50O1xuICAgIGlvbi10aXRsZSB7XG4gICAgICAgIC0tY29sb3I6IHdoaXRlO1xuICAgIH1cbn1cbi5sb2dpbi1sb2dvIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIHJpZ2h0OiAwcHg7XG4gICAgbGVmdDogMHB4O1xuICAgIG1hcmdpbi10b3A6IDMwJTtcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgIC5sb2dvX2ljb257XG4gICAgICAgIHdpZHRoOiAxMTBweCAhaW1wb3J0YW50O1xuICAgIH1cbiAgICAubG9naW4tbmFtZXtcbiAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xuICAgIH1cbiAgICAuc3ViVGl0bGV7XG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgICBmb250LXNpemU6IDFyZW07XG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgfVxufVxuLmZyZ1RhZ3tcbiAgICAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgICAgIGNvbG9yOnZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbn1cbi5idG5fY2xhc3N7XG4gICAgY29sb3I6IHdoaXRlIDtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgLS1ib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5sb2dpbi1sb2dvIGltZyB7XG4gICAgIG1heC13aWR0aDogMTUwcHg7XG59XG4ubGlzdCB7XG4gICAgbWFyZ2luLWJvdHRvbTogMDtcbn1cbmlvbi1saXN0e1xuICAgIC0taW9uLWJhY2tncm91bmQtY29sb3I6dHJhbnNwYXJlbnQ7XG4gICAgbWFyZ2luOiAwcHg7XG59XG5pb24taXRlbXtcbiAgICAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjojZjNmM2YzO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweCAhaW1wb3J0YW50O1xuICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgXG59XG4uYnRuc3tcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBwYWRkaW5nOiAxMHB4IDBweDtcbiAgICAuZmxhZ0FjdGl2ZXtcbiAgICAgICAgaGVpZ2h0OiA1MHB4O1xuICAgICAgICB3aWR0aDogNTBweDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICBib3JkZXI6IDJweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgICAgIG1hcmdpbjogMHB4IDEwcHg7XG4gICAgfVxuICAgIC5mbGFnRGVhY3RpdmV7XG4gICAgICAgIGhlaWdodDogNDVweDtcbiAgICAgICAgd2lkdGg6IDQ1cHg7XG4gICAgICAgIG1hcmdpbjogMHB4IDEwcHg7XG4gICAgICAgIG9wYWNpdHk6IC41O1xuICAgIH1cbn0gICAiLCJpb24taGVhZGVyIHtcbiAgLS1ib3gtc2hhZG93Om5vbmU7XG59XG5cbmlvbi1mb290ZXIge1xuICAtLWJveC1zaGFkb3c6bm9uZTtcbn1cblxuaW9uLXRvb2xiYXIge1xuICBib3JkZXItY29sb3I6IG5vbmUgIWltcG9ydGFudDtcbn1cbmlvbi10b29sYmFyIGlvbi10aXRsZSB7XG4gIC0tY29sb3I6IHdoaXRlO1xufVxuXG4ubG9naW4tbG9nbyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luOiBhdXRvO1xuICByaWdodDogMHB4O1xuICBsZWZ0OiAwcHg7XG4gIG1hcmdpbi10b3A6IDMwJTtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbi5sb2dpbi1sb2dvIC5sb2dvX2ljb24ge1xuICB3aWR0aDogMTEwcHggIWltcG9ydGFudDtcbn1cbi5sb2dpbi1sb2dvIC5sb2dpbi1uYW1lIHtcbiAgbWFyZ2luOiAwcHg7XG4gIGZvbnQtc2l6ZTogMS41cmVtO1xufVxuLmxvZ2luLWxvZ28gLnN1YlRpdGxlIHtcbiAgbWFyZ2luOiAwcHg7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cblxuLmZyZ1RhZyB7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xufVxuXG4uYnRuX2NsYXNzIHtcbiAgY29sb3I6IHdoaXRlO1xuICBoZWlnaHQ6IDUwcHg7XG4gIC0tYm9yZGVyLXJhZGl1czogNXB4O1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG4ubG9naW4tbG9nbyBpbWcge1xuICBtYXgtd2lkdGg6IDE1MHB4O1xufVxuXG4ubGlzdCB7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG5cbmlvbi1saXN0IHtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjp0cmFuc3BhcmVudDtcbiAgbWFyZ2luOiAwcHg7XG59XG5cbmlvbi1pdGVtIHtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjojZjNmM2YzO1xuICBib3JkZXItcmFkaXVzOiA1cHggIWltcG9ydGFudDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cblxuLmJ0bnMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcGFkZGluZzogMTBweCAwcHg7XG59XG4uYnRucyAuZmxhZ0FjdGl2ZSB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDUwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYm9yZGVyOiAycHggc29saWQgdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICBtYXJnaW46IDBweCAxMHB4O1xufVxuLmJ0bnMgLmZsYWdEZWFjdGl2ZSB7XG4gIGhlaWdodDogNDVweDtcbiAgd2lkdGg6IDQ1cHg7XG4gIG1hcmdpbjogMHB4IDEwcHg7XG4gIG9wYWNpdHk6IDAuNTtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pages/login/login.page.ts":
  /*!*******************************************!*\
    !*** ./src/app/pages/login/login.page.ts ***!
    \*******************************************/

  /*! exports provided: LoginPage */

  /***/
  function srcAppPagesLoginLoginPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LoginPage", function () {
      return LoginPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/services/api.service */
    "./src/app/services/api.service.ts");
    /* harmony import */


    var src_app_services_util_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/services/util.service */
    "./src/app/services/util.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! sweetalert2 */
    "./node_modules/sweetalert2/dist/sweetalert2.all.js");
    /* harmony import */


    var sweetalert2__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_6__);
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic-native/onesignal/ngx */
    "./node_modules/@ionic-native/onesignal/ngx/index.js");
    /* harmony import */


    var _services_window_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../services/window.service */
    "./src/app/services/window.service.ts");
    /* harmony import */


    var firebase__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! firebase */
    "./node_modules/firebase/dist/index.cjs.js");
    /* harmony import */


    var firebase__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_10__);

    var LoginPage = /*#__PURE__*/function () {
      function LoginPage(router, api, util, navCtrl, translate, oneSignal, windowService) {
        _classCallCheck(this, LoginPage);

        this.router = router;
        this.api = api;
        this.util = util;
        this.navCtrl = navCtrl;
        this.translate = translate;
        this.oneSignal = oneSignal;
        this.windowService = windowService;
        this.login = {
          email: '',
          password: ''
        };
        this.submitted = false;
        this.isLogin = false;
        this.isPhone = false;
        this.isEmail = true;
        this.isVerification = false;
        var lng = localStorage.getItem('language');

        if (!lng || lng === null) {
          localStorage.setItem('language', 'en');
        }

        this.oneSignal.getIds().then(function (data) {
          console.log('iddddd', data);
          localStorage.setItem('fcm', data.userId);
        });
        this.translate.use(localStorage.getItem('language'));
      }

      _createClass(LoginPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "validateCode",
        value: function validateCode() {
          var _this = this;

          this.windowRef.confirmationResult.confirm(this.userCode.toString()).then(function (result) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      //If the result is successful...
                      console.log('verifiedResult', result);
                      localStorage.setItem('uid', result.user.uid);
                      localStorage.setItem('help', result.user.uid);
                      this.router.navigate(['/']);

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }).catch(function (err) {
            _this.util.showToast("".concat(err), 'danger', 'bottom');
          });
        }
      }, {
        key: "onLogin",
        value: function onLogin(form) {
          var _this2 = this;

          console.log('form', form);
          this.submitted = true;

          if (form.valid) {
            var emailfilter = /^[\w._-]+[+]?[\w._-]+@[\w.-]+\.[a-zA-Z]{2,6}$/;

            if (!emailfilter.test(this.login.email)) {
              this.util.showToast(this.util.translate('Please enter valid email'), 'danger', 'bottom');
              return false;
            }

            console.log('login');
            this.isLogin = true;
            this.api.login(this.login.email, this.login.password).then(function (userData) {
              console.log(userData);

              if (userData && userData.uid) {
                _this2.api.getProfile(userData.uid).then(function (data) {
                  console.log('data', data);
                  _this2.isLogin = false;

                  if (data && data.type === 'delivery') {
                    if (data && data.status === 'active') {
                      localStorage.setItem('uid', userData.uid);
                      localStorage.setItem('help', userData.uid);
                      var lats = localStorage.getItem('lat');
                      var lngs = localStorage.getItem('lng');

                      if (lats && lngs) {
                        console.log('can update');
                        var param = {
                          lat: lats,
                          lng: lngs
                        };

                        _this2.api.updateProfile(localStorage.getItem('uid'), param).then(function (data) {
                          console.log(data);
                        }).catch(function (error) {
                          console.log(error);
                        });
                      }

                      _this2.navCtrl.navigateRoot(['/tabs']);
                    } else {
                      sweetalert2__WEBPACK_IMPORTED_MODULE_6___default.a.fire({
                        title: _this2.util.translate('Error'),
                        text: _this2.util.translate('Your are blocked please contact administrator'),
                        icon: 'error',
                        showConfirmButton: true,
                        showCancelButton: true,
                        confirmButtonText: _this2.util.translate('Need Help?'),
                        backdrop: false,
                        background: 'white'
                      }).then(function (data) {
                        if (data && data.value) {
                          localStorage.setItem('help', userData.uid);

                          _this2.router.navigate(['inbox']);
                        }
                      });
                    }
                  } else {
                    _this2.util.showToast(_this2.util.translate('your not valid user'), 'danger', 'bottom');
                  }
                }).catch(function (error) {
                  _this2.isLogin = false;
                  console.log(error);

                  _this2.util.showToast("".concat(error), 'danger', 'bottom');
                });
              }
            }).catch(function (err) {
              _this2.isLogin = false;

              if (err) {
                console.log(err);

                _this2.util.showToast("".concat(err), 'danger', 'bottom');
              }
            });
          }
        }
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.windowService.windowRef;

                  case 2:
                    this.windowRef = _context2.sent;
                    _context2.next = 5;
                    return new firebase__WEBPACK_IMPORTED_MODULE_10__["auth"].RecaptchaVerifier('recaptcha-container');

                  case 5:
                    this.windowRef.recaptchaVerifier = _context2.sent;
                    _context2.next = 8;
                    return this.windowRef.recaptchaVerifier.render();

                  case 8:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "onLoginPhone",
        value: function onLoginPhone(form) {
          var _this3 = this;

          console.log(form);
          this.api.loginPhone('3104643777', this.windowRef.recaptchaVerifier).then(function (respose) {
            console.log('response', respose);
            _this3.windowRef.confirmationResult = respose;
            _this3.isVerification = true;
          }).catch(function (res) {
            _this3.util.showToast('Verifique el captcha', 'red', 'bottom');
          });
        }
      }, {
        key: "changeMethod",
        value: function changeMethod(e) {
          console.log(e.target.value);

          if (e.target.value === "phone") {
            this.isPhone = true;
            this.isEmail = false;
          } else {
            this.isPhone = false;
            this.isEmail = true;
          }
        }
      }, {
        key: "resetPass",
        value: function resetPass() {
          this.router.navigate(['/forgot-password']);
        }
      }, {
        key: "register",
        value: function register() {
          this.router.navigate(['register']);
        }
      }, {
        key: "getClassName",
        value: function getClassName() {
          return localStorage.getItem('language');
        }
      }, {
        key: "changeLng",
        value: function changeLng(lng) {
          localStorage.setItem('language', lng);
          this.translate.use(lng);
        }
      }]);

      return LoginPage;
    }();

    LoginPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
      }, {
        type: src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"]
      }, {
        type: src_app_services_util_service__WEBPACK_IMPORTED_MODULE_3__["UtilService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateService"]
      }, {
        type: _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_8__["OneSignal"]
      }, {
        type: _services_window_service__WEBPACK_IMPORTED_MODULE_9__["WindowService"]
      }];
    };

    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-login',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./login.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./login.page.scss */
      "./src/app/pages/login/login.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], src_app_services_api_service__WEBPACK_IMPORTED_MODULE_2__["ApiService"], src_app_services_util_service__WEBPACK_IMPORTED_MODULE_3__["UtilService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateService"], _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_8__["OneSignal"], _services_window_service__WEBPACK_IMPORTED_MODULE_9__["WindowService"]])], LoginPage);
    /***/
  },

  /***/
  "./src/app/services/window.service.ts":
  /*!********************************************!*\
    !*** ./src/app/services/window.service.ts ***!
    \********************************************/

  /*! exports provided: WindowService */

  /***/
  function srcAppServicesWindowServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "WindowService", function () {
      return WindowService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var WindowService = /*#__PURE__*/function () {
      function WindowService() {
        _classCallCheck(this, WindowService);
      }

      _createClass(WindowService, [{
        key: "windowRef",
        get: function get() {
          return window;
        }
      }]);

      return WindowService;
    }();

    WindowService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], WindowService);
    /***/
  }
}]);
//# sourceMappingURL=pages-login-login-module-es5.js.map